// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs
//= require bootstrap
//= require ckeditor/init
//= require_tree .

$(document).ready(function() {
  $("#question_comment_btn").click(function() {
    $("#hidden_question_comment").toggle();
    if ($(this).text() == "comment")
      $(this).text('back');
    else
      $(this).text('comment');
  });

  $(".answer_comment_btn").click(function() {
    $(this).closest("li").find(".hidden_answer_comment").toggle();
    if ($(this).text() == "comment")
      $(this).text('back');
    else
      $(this).text('comment');
  });

  $("#change_password_btn").click(function() {
    $("#password_fields").toggle();
    if ($(this).text() == "change password")
      $(this).text("back");
    else
      $(this).text("change password");
  });

  $("#add_new_tag_btn").click(function() {
    $("#new_tag_form").toggle();
    $("#existing_tag_form").toggle();
    if ($(this).text() == "...or add new tag")
      $(this).text("back");
    else
      $(this).text("...or add new tag");
  });

  $("#add_tag_btn").click(function() {
    $("#hidden_add_tag").toggle();
    $("#add_tag_img").toggle();
    if ($(this).text() == "Add Tag")
      $(this).text('back');
    else
      $(this).text('Add Tag');  
  });

  $(".vote_button").click(function() {
    var parentDiv = $(this).closest('div'); 
    parentDiv.find('.vote_button').hide();
    parentDiv.find('.voting_gif').show();
  });

});




