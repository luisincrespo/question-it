CKEDITOR.editorConfig = function( config ) {
  config.language = 'us';
  config.uiColor = '#FFE0CC';
  config.toolbarGroups = [
  { name: 'links' },
  { name: 'insert' },
  { name: 'tools' },
  { name: 'others' },
  '/',
  { name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
  { name: 'paragraph',   groups: [ 'list', 'indent', 'align' ] },
  { name: 'styles' },
  { name: 'colors' },
  ];
};
