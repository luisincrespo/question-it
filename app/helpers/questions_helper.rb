module QuestionsHelper
  
  def clean_and_shorten(content)
    strip_tags(content[0..249] + '...').gsub(/&\w+;/, '')
  end

end
