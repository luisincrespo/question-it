module ApplicationHelper

	# Returns the full title on a per-page basis
	def full_title(page_title)
		base_title = "Question It"
		if page_title.empty?
			base_title
		else
			"#{base_title} | #{page_title}"
		end		
	end
	
	def current_path?(path)
	  request.env['PATH_INFO'] == path
	end
	
end
