module TagsHelper

	def shorten(description)
		description[0..19] + '...'
	end
end
