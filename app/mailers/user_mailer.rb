class UserMailer < ActionMailer::Base
  default from: "support@questionit.com"

  def recover_password_email(user, new_password)
    @user = user
    @new_password = new_password
    mail(to: user.email, subject: "Question It Password Recovery")
  end

end
