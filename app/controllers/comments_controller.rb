class CommentsController < ApplicationController

  before_filter :signed_in_user
  before_filter :not_admin_user


  def create
    @comment = current_user.comments.build(params[:comment])

    if params[:commentable]=='question'
      @commentable = Question.find(params[:commentable_id])
    else
      @commentable = Answer.find(params[:commentable_id])
    end

    @comment.commentable = @commentable

    if @comment.save
      if !current_user?(@comment.commentable.user)
        send_notification(@comment)
      end
      if @comment.commentable.kind_of?(Answer) && !current_user?(@comment.commentable.question.user)
        notify_questioner(@comment)
      end
      flash[:success] = "Comment posted!"
    else
      flash[:failure] = "Please write the comment"
    end

    if !@commentable.kind_of?(Question)
      redirect_to @comment.commentable.question
    else
      redirect_to @comment.commentable
    end	
  end

  private 

  def send_notification(comment)
    reported_user = comment.commentable.user
    @notification = Notification.new
    @notification.reported_user = reported_user
    @notification.issuer_user = current_user
    if comment.commentable.kind_of?(Question)
      @notification.title = "Question commented!"
      @notification.description = "A question posted by you has been commented"
      @notification.question = comment.commentable
    else
      @notification.title = "Answer commented!"
      @notification.description = "An answer posted by you has been commented"
      @notification.question = comment.commentable.question
    end
    @notification.save
  end

  def notify_questioner(comment)
    reported_user = comment.commentable.question.user
    @notification = Notification.new
    @notification.reported_user = reported_user
    @notification.issuer_user = current_user
    @notification.question = comment.commentable.question
    @notification.title = "Question's answer commented!"
    @notification.description = "An answer of a question posted by you has been commented"
    @notification.save
  end

end
