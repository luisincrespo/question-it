class TagsController < ApplicationController

  def create
    @question  = Question.find(params[:question_id])
    name_empty = params[:tag][:name].empty?
    if params[:existing] && !name_empty
      @tag = Tag.find_by_name(params[:tag][:name])
    else
      @tag = Tag.new(params[:tag])
    end

    existing = !Tag.find_by_name(params[:tag][:name]).nil?
    owned = @question.tags.include?(@tag) 
    if !owned && @question.tags << @tag
      if !current_user?(@question.user)
        send_notification(@tag, @question)
      end
      flash[:success] = "Tag added!"
    else
      flash[:failure] = "Tag owned by question" if owned
      flash[:failure] = "Tag already exists, search for existing tags" if !owned && existing
      flash[:failure] = "Tag name or description empty" if !owned && !existing
    end


    redirect_to @question
  end

  def destroy
    @tag = Tag.find(params[:id])
    @tag.destroy
    flash[:success] = "Tag destroyed"
    redirect_to tags_path
  end

  def index
    @tags = Tag.paginate(page: params[:page])
  end

  def show
    @tag = Tag.find(params[:id])
    @questions = @tag.questions.paginate(page: params[:page])
  end

  private 

  def send_notification(tag, question)
    reported_user = question.user
    @notification = Notification.new
    @notification.reported_user = reported_user
    @notification.issuer_user = current_user
    @notification.question = question
    @notification.title = "Question tagged!"
    @notification.description = "A question posted by you was tagged with the following name: \'#{tag.name}\'"
    @notification.save
  end
end
