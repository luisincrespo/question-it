class QuestionsController < ApplicationController

  before_filter :signed_in_user, only: [:new, :create, :destroy, :edit, :update]
  before_filter :correct_user, only: [:edit, :update]
  before_filter :admin_user, only: :destroy
  before_filter :not_admin_user , only: [:new, :create]

  def new
    @question = current_user.questions.build
    @tag = Tag.new
  end

  def create
    @question = current_user.questions.build(params[:question])
    if @question.save
      flash[:success] = "Question posted!"
      redirect_to @question
    else
      render 'questions/new'
    end
  end

  def destroy
    @question = Question.find(params[:id])
    @question.destroy
    flash[:success] = "Question destroyed"
    redirect_to allquestions_url
  end

  def edit
  end

  def update
    if @question.update_attributes(params[:question])
      if current_user.admin?
        flash[:success] = "Question updated"
      else
        flash[:success] = "Your question was updated"
      end
      redirect_to @question
    else
      render 'edit'
    end
  end

  def index
    @questions = Question.paginate(page: params[:page])
  end

  def show
    @question = Question.find(params[:id])
    @answer = Answer.new
    @answers = @question.answers
    @comment = Comment.new
    @tags = Tag.all.map { |tag| tag.name }
    @tag = Tag.new
  end

  def unanswered
    @all_questions = Question.paginate(page: params[:page])
    @questions = @all_questions.keep_if { |question| question.answers.count == 0 }
  end


  private

  def correct_user
    @question = Question.find(params[:id])
    @user = @question.user
    redirect_to root_path unless current_user?(@user) || current_user.admin?
  end
end
