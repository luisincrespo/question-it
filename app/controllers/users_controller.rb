class UsersController < ApplicationController

  before_filter :signed_in_user, only: [:edit, :update, :destroy, :myquestions]
  before_filter :correct_user, only: [:edit, :update]
  before_filter :admin_user, only: :destroy
  before_filter :signed_out_user, only: [:new, :create, :new_password, :create_password]
  before_filter :not_admin_user , only: :myquestions

  def new
    @user = User.new
  end

  def create
    @user = User.new(params[:user])
    @user.setting_password = true
    if @user.save
      sign_in @user
      flash[:success] = "Welcome to Question It!"
      redirect_to @user
    else
      render 'new'
    end
  end

  def show
    @user = User.find(params[:id])
  end

  def edit
  end

  def update
    @user.attributes = params[:user]
    @user.setting_password = false
    if !@user.password.empty? || !@user.password_confirmation.empty?
      @user.setting_password = true
    end
    if @user.save
      if current_user?(@user) 
        flash[:success] = "Your information was updated!"
        sign_in @user
      else
        flash[:success] = "Updated!"
      end
      redirect_to @user
    else
      render 'edit'
    end
  end

  def destroy
    @user = User.find(params[:id])
    unless current_user?(@user)
      name_user_destroyed = @user.name
      @user.destroy
      flash[:success] = "#{name_user_destroyed} destroyed"
      redirect_to allusers_path
    end
  end

  def index
    @users = User.paginate(page: params[:page])
  end

  def myquestions
    @questions = current_user.questions.paginate(page: params[:page])
  end

  def new_password
    @user = User.new 
  end

  def create_password
    @user = User.find_by_email(params[:email])
    password = (0...8).map{(65+rand(26)).chr}.join 
    if @user.nil?
      flash.now[:failure] = "E-mail does not belong to Question It community"
      render 'new_password'
    else
      @user.password = password
      @user.password_confirmation = password
      @user.save
      UserMailer.recover_password_email(@user, password).deliver
      flash[:success] = "New password sent to e-mail"
      redirect_to signin_path
    end
  end

  private

  def signed_out_user
    redirect_to root_url unless !signed_in?
  end

  def correct_user
    @user = User.find(params[:id])
    redirect_to root_path unless current_user?(@user) || current_user.admin?
  end
end
