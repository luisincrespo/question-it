class NotificationsController < ApplicationController
  before_filter :signed_in_user
  before_filter :not_admin_user
  
  def index
    @notifications = current_user.received_notifications.paginate(page: params[:page])
  end

  def erase_notification
    @notification = Notification.find(params[:id])
    @question = @notification.question
    @notification.destroy

    redirect_to @question
  end
end
