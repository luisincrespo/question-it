class AnswersController < ApplicationController

  before_filter :signed_in_user
  before_filter :not_admin_user

  def create
    @answer = current_user.answers.build(params[:answer])
    @answer.question = Question.find(params[:question_id])
    if @answer.save
      if !current_user?(@answer.question.user)
        send_notification(@answer)
      end
      flash[:success] = "Answer posted!"
    else
      flash[:failure] = "Please write the answer"
    end


    redirect_to @answer.question
  end

  def vote_plus
    @answer = Answer.find(params[:id])
    if !@answer.voting_users.include?(current_user)
      @answer.vote = @answer.vote + 1
      @answer.save
      @answer.voting_users << current_user
      if !current_user?(@answer.user)
        send_voteup_notification(@answer)
      end
      if !current_user?(@answer.question.user)
        notify_voteup_questioner(@answer)
      end
      flash[:success] = "Positive vote posted!"
    else
      flash[:failure] = "You've already voted for that answer"
    end

    redirect_to @answer.question
  end

  def vote_minus
    @answer = Answer.find(params[:id])
    if !@answer.voting_users.include?(current_user)
      @answer.vote = @answer.vote - 1
      @answer.save
      @answer.voting_users << current_user
      if !current_user?(@answer.user)
        send_votedown_notification(@answer)
      end
      if !current_user?(@answer.question.user)
        notify_votedown_questioner(@answer)
      end
      flash[:success] = "Negative vote posted!"
    else
      flash[:failure] = "You've already voted for that answer"
    end

    redirect_to @answer.question
  end

  def best_answer
    @answer = Answer.find(params[:id])
    @answer.question.answers.each do |answer|
      if answer.best_answer
        answer.best_answer = false
        answer.save
      end	
    end
    @answer.best_answer = true
    @answer.save

    send_bestanswer_notification(@answer)

    redirect_to @answer.question
  end

  private

  def send_notification(answer)
    reported_user = answer.question.user
    @notification = Notification.new
    @notification.reported_user = reported_user
    @notification.issuer_user = current_user
    @notification.question = answer.question
    @notification.title = "Question answered!"
    @notification.description = "A question posted by you has been answered"
    @notification.save
  end

  def send_voteup_notification(answer)
    reported_user = answer.user
    @notification = Notification.new
    @notification.reported_user = reported_user
    @notification.issuer_user = current_user
    @notification.question = answer.question
    @notification.title = "Answer voted up!"
    @notification.description = "An answer posted by you has been voted up"
    @notification.save
  end

  def send_votedown_notification(answer)
    reported_user = answer.user
    @notification = Notification.new
    @notification.reported_user = reported_user
    @notification.issuer_user = current_user
    @notification.question = answer.question
    @notification.title = "Answer voted down!"
    @notification.description = "An answer posted by you has been voted down"
    @notification.save
  end

  def notify_voteup_questioner(answer)
    reported_user = answer.question.user
    @notification = Notification.new
    @notification.reported_user = reported_user
    @notification.issuer_user = current_user
    @notification.question = answer.question
    @notification.title = "Question's answer voted up!"
    @notification.description = "An answer of a question posted by you has been voted up"
    @notification.save
  end

  def notify_votedown_questioner(answer)
    reported_user = answer.question.user
    @notification = Notification.new
    @notification.reported_user = reported_user
    @notification.issuer_user = current_user
    @notification.question = answer.question
    @notification.title = "Question's answer voted down!"
    @notification.description = "An answer of a question posted by you has been voted down"
    @notification.save
  end

  def send_bestanswer_notification(answer)
    reported_user = answer.user
    @notification = Notification.new
    @notification.reported_user = reported_user
    @notification.issuer_user = current_user
    @notification.question = answer.question
    @notification.title = "Answer selected as best answer!"
    @notification.description = "Your answer for a question has been selected as the best answer"
    @notification.save
  end

end
