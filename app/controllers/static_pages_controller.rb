class StaticPagesController < ApplicationController
  def home
    @all_questions = Question.paginate(page: params[:page])
    seven_days_ago = DateTime.now - 7
    @questions = @all_questions.keep_if { |question| !question.answers.empty? && question.answers.sort_by(&:created_at).last.created_at >= seven_days_ago }
  end

  def help
  end

  def about
  end

  def contact
  end
end
