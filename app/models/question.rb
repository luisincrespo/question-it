# == Schema Information
#
# Table name: questions
#
#  id         :integer          not null, primary key
#  title      :string(255)
#  content    :text
#  user_id    :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class Question < ActiveRecord::Base
  attr_accessible :content, :title
  belongs_to :user
  has_many :answers, dependent: :destroy
  has_many :comments, dependent: :destroy, :as => :commentable
  has_and_belongs_to_many :tags
  has_many :notifications, dependent: :destroy
  
  before_save { |question| question.title = title.downcase.capitalize }
  
  validates :user_id, presence: true
  validates :content, presence: true
  validates :title, presence: true, length: { maximum: 100 }
  
  default_scope order: 'questions.created_at DESC'
end
