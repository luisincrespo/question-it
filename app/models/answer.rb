# == Schema Information
#
# Table name: answers
#
#  id          :integer          not null, primary key
#  content     :text
#  user_id     :integer
#  question_id :integer
#  vote        :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  best_answer :boolean          default(FALSE)
#

class Answer < ActiveRecord::Base
  attr_accessible :content, :vote, :best_answer
  belongs_to :user
  belongs_to :question
  has_many :comments, dependent: :destroy, :as => :commentable
  has_and_belongs_to_many :voting_users, class_name: 'User'

  before_save :default_values

  validates :user_id, presence: true
  validates :question_id, presence: true
  validates :content, presence: true

  default_scope order: 'answers.vote DESC'

  private

  def default_values
    self.vote ||= 0
  end

end
