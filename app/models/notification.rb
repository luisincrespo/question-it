# == Schema Information
#
# Table name: notifications
#
#  id               :integer          not null, primary key
#  reported_user_id :integer
#  issuer_user_id   :integer
#  question_id      :integer
#  title            :string(255)
#  description      :text
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#

class Notification < ActiveRecord::Base
  attr_accessible :description, :title
  belongs_to :reported_user, :class_name => 'User', :foreign_key => 'reported_user_id'
  belongs_to :issuer_user, :class_name => 'User', :foreign_key => 'issuer_user_id'
  belongs_to :question

  validates :title, presence: true
  validates :description, presence: true

  default_scope order: 'notifications.created_at DESC'
end
