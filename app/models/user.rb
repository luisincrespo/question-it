# == Schema Information
#
# Table name: users
#
#  id              :integer          not null, primary key
#  name            :string(255)
#  email           :string(255)
#  password_digest :string(255)
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  country         :string(255)
#  city            :string(255)
#  birthdate       :date
#  remember_token  :string(255)
#  admin           :boolean          default(FALSE)
#

class User < ActiveRecord::Base
	attr_accessible :email, :name, :password , :password_confirmation, :country, :city, :birthdate
  attr_accessor :setting_password
	has_secure_password
	has_many :questions, dependent: :destroy
	has_many :answers, dependent: :destroy
	has_many :comments, dependent: :destroy
  has_and_belongs_to_many :voted_answers, class_name: 'Answer'
  has_many :received_notifications, dependent: :destroy, :class_name => 'Notification', :foreign_key => 'reported_user_id'
  has_many :issued_notifications, dependent: :destroy, :class_name => 'Notification', :foreign_key => 'issuer_user_id'
	
	before_save do |user|
		user.name = user.name.downcase.gsub(/\s+/, ' ').split(' ').map(&:capitalize).join(' ')
		email.downcase!
		user.country = user.country.downcase.gsub(/\s+/, ' ').split(' ').map(&:capitalize).join(' ')
		user.city = user.city.downcase.gsub(/\s+/, ' ').split(' ').map(&:capitalize).join(' ')
	end
	
	before_save :create_remember_token
	
	validates :name, presence: true, length: { maximum: 50 }
	VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
	validates :email, presence: true, format: { with: VALID_EMAIL_REGEX },
    uniqueness: { case_sensitive: false }
	
	validates :password, length: { minimum: 6 }, if: :setting_password 
	validates :password_confirmation, presence: true, if: :setting_password 
	
	validates :country, presence: true, length: { maximum: 50 }
	validates :city, presence: true, length: { maximum: 50 }
	validates :birthdate, presence: true
	
	private
	
		def create_remember_token
			self.remember_token = SecureRandom.urlsafe_base64
		end
end
