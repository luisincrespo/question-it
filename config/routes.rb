QuestionIt::Application.routes.draw do

  get "notifications/index"

  resources :users
  resources :sessions, only: [:new, :create, :destroy]
  resources :questions
  resources :answers, only: [:create, :destroy]
  resources :comments, only: [:create, :destroy]
  resources :tags, only: [:create, :destroy, :show]
  resources :notifications, only: :index

  root to: 'static_pages#home' 
  
  match '/signup', to: 'users#new'
  match '/myquestions', to: 'users#myquestions'
  match '/allusers', to: 'users#index'
  match '/newpassword', to: 'users#new_password'
  match '/changepassword', to: 'users#create_password', via: :post
  match '/signout', to: 'sessions#destroy', via: :delete
  match '/signin', to: 'sessions#new'
  match '/newquestion', to: 'questions#new'
  match '/allquestions', to: 'questions#index'
  match '/unanswered', to: 'questions#unanswered'
  match '/tags', to: 'tags#index'
  match '/voteplus', to: 'answers#vote_plus', via: :put
  match '/voteminus', to: 'answers#vote_minus', via: :put
  match '/bestanswer', to: 'answers#best_answer', via: :put
  match '/erasenotification', to: 'notifications#erase_notification', via: :put
 

  match '/help', to: 'static_pages#help'
  match '/about', to: 'static_pages#about'
  match '/contact', to: 'static_pages#contact'
 
  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => 'welcome#index'

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id))(.:format)'
end
