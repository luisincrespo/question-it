class AddIndexQuestionsTags < ActiveRecord::Migration
  def change
	add_index :questions_tags, [:question_id, :tag_id]
  end

  def down
  end
end
