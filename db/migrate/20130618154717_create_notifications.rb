class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.integer :reported_user_id
      t.integer :issuer_user_id
      t.integer :question_id
      t.string :title
      t.text :description

      t.timestamps
    end
  end
end
