class AddBirthdateDeleteAgeUser < ActiveRecord::Migration
  def change
	add_column :users, :birthdate, :date
	remove_column :users, :age
  end
end
