class CreateVotingusersVotedanswersJoin < ActiveRecord::Migration
  def up
    create_table 'answers_users', :id => false do |t|
      t.column 'answer_id', :integer
      t.column 'user_id', :integer
    end
  end

  def down
    drop_table 'answers_users'
  end
end
