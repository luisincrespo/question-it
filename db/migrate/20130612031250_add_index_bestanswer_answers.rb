class AddIndexBestanswerAnswers < ActiveRecord::Migration
  def up
    add_index :answers, :best_answer
  end

  def down
    remove_index :answers, :best_answer
  end
end
