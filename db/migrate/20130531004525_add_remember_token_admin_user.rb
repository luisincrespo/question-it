class AddRememberTokenAdminUser < ActiveRecord::Migration
  def change
	add_column :users, :remember_token, :string
	add_column :users, :admin, :boolean, default: false
  end
end
