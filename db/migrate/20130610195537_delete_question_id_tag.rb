class DeleteQuestionIdTag < ActiveRecord::Migration
  def change
	remove_column :tags, :question_id
  end

  def down
  end
end
